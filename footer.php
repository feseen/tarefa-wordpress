<footer>
    <div id="gradient"></div>
    <div id="footer">
        <div id="informations">
            <iframe id="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9532.642908289234!2d-43.13159866279359!3d-22.90096450626681!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817ee1756031%3A0xd1dcbde0df6f873c!2sAv.%20Milton%20Tavares%20de%20Souza%20-%20Gragoat%C3%A1%2C%20Niter%C3%B3i%20-%20RJ%2C%2024210-346!5e0!3m2!1spt-BR!2sbr!4v1659204297499!5m2!1spt-BR!2sbr" loading="lazy"></iframe>
            <div id="contact">
                <div class="icon-text">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/footer/location.svg" class="footer-icon"> <p>Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</p>
                </div>
                <div class="icon-text">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/footer/phone.svg" class="footer-icon"> <p>(99) 99999-9999</p>
                </div>
                <div class="icon-text">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/images/footer/email.svg" class="footer-icon"> <p>salve-lobos@lobINhos.com</p>
                </div>
                <button id="footer-button">Quem Somos</button>
            </div>
        </div>
        <div id="made-with">
            <p>Desenvolvido com</p>
            <img src="<?php echo get_stylesheet_directory_uri() ?>/images/footer/paws.png">
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>