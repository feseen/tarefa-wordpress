<?php 
    function queue_css(){
        wp_enqueue_style('style', get_stylesheet_uri());
        global $template;
        if(basename($template) == "home.php"){
            wp_enqueue_style( 'home', get_stylesheet_directory_uri().'/css/home.css' );
        }
        else if(basename($template) == "wolf-list.php"){
            wp_enqueue_style( 'wolf-list', get_stylesheet_directory_uri().'/css/wolf-list.css' );
        }
        else if(basename($template) == "who-we-are.php"){
            wp_enqueue_style( 'who-we-are', get_stylesheet_directory_uri().'/css/who-we-are.css' );
        }
    }
    add_action('wp_enqueue_scripts', 'queue_css');
?>