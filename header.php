<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Adote um Lobinho
    </title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <div class="header-link">
            <a id="first-link" href="<?php echo 'wolf-list' ?>">Nossos Lobinhos<div class="header-link-underline"></div></a>
                
            </div>
            <div class="header-link">
                <a href="<?php echo 'home' ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/wolf_icon.svg"></a>
            </div>
            <div class="header-link">
                <a href="<?php echo 'who-we-are' ?>">Quem Somos<div id="second-link" class="header-link-underline"></div></a>      
        </div>
    </header>