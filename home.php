<?php
// Template Name: Home
?>

<?php get_header();?>
<main>
    <div id="title-section">
        <h1 id="title"><?php the_field('main_title'); ?></h1>
            <div id="title-underline"></div>
        
        <p id="subtitle"><?php the_field('main_description'); ?></p>
    </div>
    <div id="about">
        <h2><?php the_field('about_title'); ?></h2>
        <p><?php the_field('about_description'); ?></p>
    </div>
    <div id="values">
        <h2><?php the_field('values_values_title'); ?></h2>
        <div class="value">
            <div class="ellipse"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/homepage/protection.svg" class="value-icon"></div>
            <h3 class="value-title"><?php the_field('values_first_value_title'); ?></h3>
            <p class="value-description">
                <?php the_field('values_first_value_description'); ?>
            </p>
        </div>
        <div class="value">
            <div class="ellipse"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/homepage/care.svg" class="value-icon"></div>
            <h3 class="value-title"><?php the_field('values_second_value_title'); ?></h3>
            <p class="value-description">
                <?php the_field('values_second_value_description'); ?>
            </p>
        </div>
        <div class="value">
            <div class="ellipse"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/homepage/companionship.svg" class="value-icon"></div>
            <h3 class="value-title"><?php the_field('values_third_value_title'); ?></h3>
            <p class="value-description">
                <?php the_field('values_third_value_description'); ?>
            </p>
        </div>
        <div class="value">
            <div class="ellipse"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/homepage/rescue.svg" class="value-icon"></div>
            <h3 class="value-title"><?php the_field('values_forth_value_title'); ?></h3>
            <p class="value-description">
                <?php the_field('values_forth_value_description'); ?>
            </p>
        </div>
    </div>
    <div id="wolves-example">
        <h2><?php the_field('wolves_examples_title') ?></h2>

        <?php 
            $query = new WP_Query(array ( 'orderby' => 'rand', 'posts_per_page' => 2 ));
            $toggle_left = false;
            while($query->have_posts()) : $query->the_post();
            if($toggle_left = !$toggle_left):
        ?>
        <div class="wolf left">
            <div class="wolf-portrait">
                <?php 
                    if(get_field('wolf_image')):
                ?>
                    <img class="wolf-img" src="<?php the_field('wolf_image'); ?>">
                <?php 
                    endif;
                ?>
                <div class="wolf-img-shadow"></div>
            </div>
            <div class="wolf-info">
                <div class="wolf-name-age">
                    <p class="wolf-name"> <?php the_field('wolf_name'); ?> </p>
                    <p class="wolf-age">Idade: <?php the_field('wolf_age'); ?> anos</p>
                </div>
                <div class="wolf-desc">
                    <p><?php the_field('wolf_description'); ?></p>
                </div>
            </div>   
        </div>
        <?php 
            else:
        ?>
        <div class="wolf right">
            <div class="wolf-info">
                <div class="wolf-name-age">
                    <p class="wolf-name"><?php the_field('wolf_name'); ?></p>
                    <p class="wolf-age">Idade: <?php the_field('wolf_age'); ?> anos</p>
                </div>
                <div class="wolf-desc">
                    <p><?php the_field('wolf_description'); ?></p>
                </div>
            </div>   
            <div class="wolf-portrait">
                <?php 
                    if(get_field('wolf_image')):
                ?>
                    <img class="wolf-img" src="<?php the_field('wolf_image'); ?>">
                <?php 
                    endif;
                ?>
                <div class="wolf-img-shadow"></div>
            </div>
        </div>

        <?php 
            endif; endwhile;
            wp_reset_postdata();
        ?>

    </div>
</main>
<?php get_footer();?>