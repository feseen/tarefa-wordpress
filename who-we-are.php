<?php
// Template Name: Who We Are
?>

<?php get_header();?>
<main>
    <?php echo the_title('<h1>', '</h1>') ?>
    <p><?php echo the_content() ?></p>
</main>
<?php get_footer();?>