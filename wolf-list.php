<?php
// Template Name: Wolf List
?>
<?php get_header();?>

<main>
    <div id="search-add">
        <div id="search-bar">
            <img id="search-icon" src="<?php echo get_stylesheet_directory_uri() ?>/images/search.svg">
            <input type="text" id="search">
        </div>
        <button id="add">+ Lobo</button>
    </div>
    <div id="checkbox-label">
        <input type="checkbox" id="see-adopted-only">
        <label id="checkbox-text">Ver lobinhos adotados</label>
    </div>
    <div id="wolves">
        <?php 
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
            $query = new WP_Query( array(
                'posts_per_page' => 4,
                'paged' => $paged
            ) );
            $toggle_left = false;
        ?>
        <?php while($query->have_posts()) : $query->the_post(); ?>
        <?php    if($toggle_left = !$toggle_left): ?>
            <div class="wolf left">
                <div class="wolf-portrait">
                    <?php 
                        if(get_field('wolf_image')):
                    ?>
                        <img class="wolf-img" src="<?php the_field('wolf_image'); ?>">
                    <?php 
                        endif;
                    ?>
                    <div class="wolf-img-shadow"></div>
                </div>
                <div class="wolf-info">
                    <div class="wolf-name-adopt">
                        <div class="wolf-name-age">
                            <p class="wolf-name"><?php the_field('wolf_name'); ?></p>
                            <p class="wolf-age">Idade: <?php the_field('wolf_age'); ?> anos</p>
                        </div>
                        <button class="adopt to-adopt">Adotar</button>
                    </div>
                    <div class="wolf-desc">
                        <p><?php the_field('wolf_description') ?></p>
                    </div>
                </div>   
            </div>
            <?php 
                else:
            ?>

            <div class="wolf right">
                <div class="wolf-info">
                    <div class="wolf-name-adopt">
                        <div class="wolf-name-age">
                            <p class="wolf-name"><?php the_field('wolf_name'); ?></p>
                            <p class="wolf-age">Idade: <?php the_field('wolf_age'); ?> anos</p>
                        </div>
                        <button class="adopt to-adopt">Adotar</button>
                    </div>
                    <div class="wolf-desc">
                        <p><?php the_field('wolf_description') ?></p>
                    </div>
                </div>
                <div class="wolf-portrait">
                    <?php 
                        if(get_field('wolf_image')):
                    ?>
                        <img class="wolf-img" src="<?php the_field('wolf_image'); ?>">
                    <?php 
                        endif;
                    ?>
                    <div class="wolf-img-shadow"></div>
                </div>   
            </div>

        <?php endif; endwhile; ?>
        <div class="pagination">
    <?php
        echo paginate_links( array(
            'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
            'total'        => $query->max_num_pages,
            'current'      => max( 1, get_query_var( 'paged' ) ),
            'format'       => '?paged=%#%',
            'show_all'     => false,
            'type'         => 'plain',
            'end_size'     => 2,
            'mid_size'     => 1,
            'prev_next'    => true,
            'prev_text'    => sprintf( '<i></i> %1$s', __( '<<', 'text-domain' ) ),
            'next_text'    => sprintf( '%1$s <i></i>', __( '>>', 'text-domain' ) ),
            'add_args'     => false,
            'add_fragment' => '',
        ) );
    ?>
    <?php wp_reset_postdata(); ?>
</div>
    </div>
</main>

<?php get_footer();?>